package net.domdiidom.wizard.main;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;



public class PlayerStats {
	private int kills, deaths;
	public boolean isFireballReady, isShieldReady, isTeleportationReady, isIceRainReady;
	private Main main;
	private Player player;
	private Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
	private Objective obj = board.registerNewObjective("aaa", "bbb");
	

	
	int g, h, i, j, k, l;
	
	private Score fireball = obj.getScore("�3Fireball");
	private Score teleportation = obj.getScore("�3Teleportation");
	private Score shield = obj.getScore("�3Shield");
	
	@SuppressWarnings("deprecation")
	public PlayerStats(Main main, Player player){
		isFireballReady = true;
		isShieldReady = true;
		isTeleportationReady = true;
		isIceRainReady = true;
		
		this.main = main;
		this.player = player;
		
		obj.setDisplayName("Spell Overview");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		fireball.setScore(0);
		teleportation.setScore(0);
		shield.setScore(0);
		
		player.setScoreboard(board);
		
		player.setLevel(100);
		
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(main, new Runnable(){
			@Override
			public void run(){
				int current = player.getLevel();
				if(current < 100){
					current += main.getConfig().getInt("mana_regeneration");
					player.setLevel(current);
				}
			}
		}, 20L, 20L);
	}
	
	@SuppressWarnings("deprecation")
	public void onUseFireball(){
		player.setLevel(player.getLevel() - main.getConfig().getInt("spell.fireball.mana"));
		isFireballReady = false;
		Bukkit.getScheduler().scheduleAsyncDelayedTask(main, new Runnable(){
			@Override
			public void run(){			
				int cooldown = main.getConfig().getInt("spell.fireball.cooldown");
				
				try{
					while(cooldown != 0){
						obj.getScore("�3Fireball").setScore(cooldown);
						cooldown--;
						Thread.sleep(1000);
					}
					obj.getScore("�3Fireball").setScore(0);
					isFireballReady = true;
				}catch(Exception e){e.printStackTrace();}
			}
		});
	}
	
	@SuppressWarnings("deprecation")
	public void onUseShield(){
		player.setLevel(player.getLevel() - main.getConfig().getInt("spell.shield.mana"));
		isShieldReady = false;
		Bukkit.getScheduler().scheduleAsyncDelayedTask(main, new Runnable(){
			@Override
			public void run(){			
				int cooldown = main.getConfig().getInt("spell.shield.cooldown");
				
				try{
					while(cooldown != 0){
						obj.getScore("�3Shield").setScore(cooldown);
						cooldown--;
						Thread.sleep(1000);
					}
					obj.getScore("�3Shield").setScore(0);
					isShieldReady = true;
				}catch(Exception e){e.printStackTrace();}
			}
		});
	}
	
	@SuppressWarnings("deprecation")
	public void onUseTeleportation(){
		player.setLevel(player.getLevel() - main.getConfig().getInt("spell.teleportation.mana"));
		isTeleportationReady = false;
		Bukkit.getScheduler().scheduleAsyncDelayedTask(main, new Runnable(){
			@Override
			public void run(){			
				int cooldown = main.getConfig().getInt("spell.teleportation.cooldown");
				
				try{
					while(cooldown != 0){
						obj.getScore("�3Teleportation").setScore(cooldown);
						cooldown--;
						Thread.sleep(1000);
					}
					obj.getScore("�3Teleportation").setScore(0);
					isTeleportationReady = true;
				}catch(Exception e){e.printStackTrace();}
			}
		});
	}
	
	public void onPlayerJoinMatch(){
		SpawnManager.onPlayerSpawn(player, main);
		Bukkit.broadcastMessage("�3" + player.getName() + " ist dem Spiel beigetreten!");
		player.getInventory().clear();
		Inventory inventory = player.getInventory();
		inventory.addItem(new ItemStack(Material.BLAZE_ROD));
		inventory.addItem(new ItemStack(Material.SHIELD));
		inventory.addItem(new ItemStack(Material.FEATHER));
		
		ItemStack item = new ItemStack(Material.SLIME_BALL);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("�4LEAVE");
		item.setItemMeta(meta);
		inventory.setItem(8, item);
	}
	
	
}
