package net.domdiidom.wizard.main;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.domdiidom.wizard.commands.WizardCommand;
import net.domdiidom.wizard.events.EventListener;

public class Main extends JavaPlugin{
	
	ConsoleCommandSender console = Bukkit.getConsoleSender();
	
	private Location a = null;
	private Location b = null;
	private Location c = null;
	private Location d = null;
	private Location e = null;
	private Location f = null;
	
	public int superX, superY, superZ;
	
	public List<Match> match = new LinkedList<Match>();
	public HashMap<Player, PlayerStats> stats = new HashMap<Player, PlayerStats>();
	
	
	@Override
	public void onEnable(){
		console.sendMessage("�2[Wizard] Enabled!");
		
		SpawnManager.hash.clear();
		SpawnManager.hash.put("a", new SpawnOverwatch());
		SpawnManager.hash.put("b", new SpawnOverwatch());
		SpawnManager.hash.put("c", new SpawnOverwatch());
		SpawnManager.hash.put("d", new SpawnOverwatch());
		SpawnManager.hash.put("e", new SpawnOverwatch());
		SpawnManager.hash.put("f", new SpawnOverwatch());
		SpawnManager.hash.put("g", new SpawnOverwatch());
		SpawnManager.hash.put("h", new SpawnOverwatch());
		
		this.onRegisterEvents();
		this.onLoadConfig();
		this.onRegisterCommand();
		Bukkit.getWorld("Arena").setSpawnLocation(this.getConfig().getInt("map.s.x"), this.getConfig().getInt("map.s.y"), this.getConfig().getInt("map.s.z"));
		
		superX = this.getConfig().getInt("map.super.x");
		superY = this.getConfig().getInt("map.super.y");
		superZ = this.getConfig().getInt("map.super.z");
		
		for(Player p : Bukkit.getOnlinePlayers()){
			if(!stats.containsKey(p))
				stats.put(p, new PlayerStats(this, p));
		}		
	}
	
	@Override
	public void onDisable(){
		console.sendMessage("�4[Wizard] Disblaed!");
	}
	
	public void onRegisterEvents(){
		System.out.println("EVENTS");
		this.getServer().getPluginManager().registerEvents(new EventListener(this), this);
	}
	
	public static String getCardinalDirection(Player player){
		try{
			double rotation = (player.getLocation().getYaw() - 90) % 360;
			if (rotation < 0)
			    rotation += 360.0;
			if (0 <= rotation && rotation < 67.5)
			    return "NW"; //NorthWest
			else if (67.5 <= rotation && rotation < 112.5)
			    return "N"; //North 
			else if (112.5 <= rotation && rotation < 157.5)
			    return "NE"; //NorthEeast
			else if (157.5 <= rotation && rotation < 202.5)
			    return "E"; //East 
			else if (202.5 <= rotation && rotation < 247.5)
			        return "SE"; //SouthEast 
			else if (247.5 <= rotation && rotation < 292.5)
			    return "S"; //South 
			else if (292.5 <= rotation && rotation < 337.5)
			    return "SW"; //SouthWest 
			else if (337.5 <= rotation && rotation < 360.0)
			   return "W"; //West 			   
		}catch(Exception e){e.printStackTrace();}
		return null;
	}
	

	
	public void onLoadConfig(){
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
	}
	
	public void onRegisterCommand(){
		this.getCommand("wizard").setExecutor(new WizardCommand(this));
	}
	
	public void North(World world, Location loc){
		a = new Location(world, loc.getBlockX()+1, loc.getBlockY()+1, loc.getBlockZ()-1);
		b = new Location(world, loc.getBlockX(), loc.getBlockY()+1, loc.getBlockZ()-1);
		c = new Location(world, loc.getBlockX()-1, loc.getBlockY()+1, loc.getBlockZ()-1);		
		
		d = new Location(world, loc.getBlockX()+1, loc.getBlockY(), loc.getBlockZ()-1);
		e = new Location(world, loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()-1);
		f = new Location(world, loc.getBlockX()-1, loc.getBlockY(), loc.getBlockZ()-1);
				
		this.onDespawnShield(a, b, c, d, e, f);
	}
	
	public void South(World world, Location loc){
		a = new Location(world, loc.getBlockX()+1, loc.getBlockY()+1, loc.getBlockZ()+1);
		b = new Location(world, loc.getBlockX(), loc.getBlockY()+1, loc.getBlockZ()+1);
		c = new Location(world, loc.getBlockX()-1, loc.getBlockY()+1, loc.getBlockZ()+1);		
		
		d = new Location(world, loc.getBlockX()+1, loc.getBlockY(), loc.getBlockZ()+1);
		e = new Location(world, loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()+1);
		f = new Location(world, loc.getBlockX()-1, loc.getBlockY(), loc.getBlockZ()+1);
				
		this.onDespawnShield(a, b, c, d, e, f);
	}
	
	public void East(World world, Location loc){
		a = new Location(world, loc.getBlockX()+1, loc.getBlockY()+1, loc.getBlockZ()-1);
		b = new Location(world, loc.getBlockX()+1, loc.getBlockY()+1, loc.getBlockZ());
		c = new Location(world, loc.getBlockX()+1, loc.getBlockY()+1, loc.getBlockZ()+1);		
		
		d = new Location(world, loc.getBlockX()+1, loc.getBlockY(), loc.getBlockZ()-1);
		e = new Location(world, loc.getBlockX()+1, loc.getBlockY(), loc.getBlockZ());
		f = new Location(world, loc.getBlockX()+1, loc.getBlockY(), loc.getBlockZ()+1);	
				
		this.onDespawnShield(a, b, c, d, e, f);
	}
	
	public void West(World world, Location loc){
		a = new Location(world, loc.getBlockX()-1, loc.getBlockY()+1, loc.getBlockZ()-1);
		b = new Location(world, loc.getBlockX()-1, loc.getBlockY()+1, loc.getBlockZ());
		c = new Location(world, loc.getBlockX()-1, loc.getBlockY()+1, loc.getBlockZ()+1);		
		
		d = new Location(world, loc.getBlockX()-1, loc.getBlockY(), loc.getBlockZ()-1);
		e = new Location(world, loc.getBlockX()-1, loc.getBlockY(), loc.getBlockZ());
		f = new Location(world, loc.getBlockX()-1, loc.getBlockY(), loc.getBlockZ()+1);	
				
		this.onDespawnShield(a, b, c, d, e, f);	
	}
	
	@SuppressWarnings("deprecation")
	public void onSpawnShield(){
		a.getBlock().setType(Material.DIAMOND_BLOCK);
		b.getBlock().setType(Material.DIAMOND_BLOCK);
		c.getBlock().setType(Material.DIAMOND_BLOCK);
		d.getBlock().setType(Material.DIAMOND_BLOCK);
		e.getBlock().setType(Material.DIAMOND_BLOCK);
		f.getBlock().setType(Material.DIAMOND_BLOCK);	
		this.onDespawnShield(a, b, c, d, e, f);	
	}
	
	public void onDespawnShield(Location g1, Location h1, Location i1, Location j1, Location k1, Location l1){
		Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
			@Override
			public void run(){
				g1.getBlock().setType(Material.AIR);
				h1.getBlock().setType(Material.AIR);
				i1.getBlock().setType(Material.AIR);
				j1.getBlock().setType(Material.AIR);
				k1.getBlock().setType(Material.AIR);
				l1.getBlock().setType(Material.AIR);
			}
		}, 20 * 4);
	}
}
