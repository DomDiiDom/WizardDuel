package net.domdiidom.wizard.main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Super {
	public static boolean isCharging = false;
	
	private static String player;
	private static Main main;
	
	private static Location loc;
	
	@SuppressWarnings("deprecation")
	public static void onEnabled(String s, Main m){
		player = s;
		main = m;
		
		isCharging = true;
		
		Bukkit.broadcastMessage("Der Spieler " + player + " wird aufgeladen!");
		
		onToggleBeacon(true);
		onToggleWeather(true);
		onToggleTime(true);
		onStrike();
		
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){
			@Override
			public void run(){
				onToggleBeacon(false);
				onToggleWeather(false);
				onToggleTime(false);
				Bukkit.broadcastMessage("Super beendet!");
				isCharging = false;
			}
		}, 20 * 60L);
		
	}
	
	private static void onToggleTime(boolean b){
		if(b){
			Bukkit.getPlayer(player).getWorld().setTime(13000);
		}else{
			Bukkit.getPlayer(player).getWorld().setTime(1000);
		}
	}
	
	private static void onToggleBeacon(boolean b){
		loc = new Location(Bukkit.getPlayer(player).getWorld(), main.superX, main.superY-1, main.superZ);
		if(b){
			loc.getBlock().setType(Material.BEACON);
			Location l = new Location(Bukkit.getPlayer(player).getWorld(), main.superX, main.superY-2, main.superZ);
			l.getBlock().setType(Material.DIRT);
		}
		else{
			loc.getBlock().setType(Material.STONE);
			Location l = new Location(Bukkit.getPlayer(player).getWorld(), main.superX, main.superY-2, main.superZ);
			l.getBlock().setType(Material.STONE);
		}
		
	}
	
	private static void onToggleWeather(boolean b){
		if(b){
			Bukkit.getPlayer(player).getWorld().setStorm(true);
			Bukkit.getPlayer(player).getWorld().setThundering(true);
		}else{
			Bukkit.getPlayer(player).getWorld().setStorm(false);
			Bukkit.getPlayer(player).getWorld().setThundering(false);
		}
			
	}
	
	@SuppressWarnings("deprecation")
	private static void onStrike(){
		Bukkit.getScheduler().scheduleAsyncDelayedTask(main, new Runnable(){
			@Override
			public void run(){
				Bukkit.getPlayer(player).setNoDamageTicks(20*5);
				Bukkit.getPlayer(player).getWorld().strikeLightning(new Location(Bukkit.getPlayer(player).getWorld(), main.superX, main.superY-2, main.superZ));
				try{Thread.sleep(750);}catch(Exception e){}
				SpawnManager.onPlayerSpawn(Bukkit.getPlayer(player), main);
				Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){@Override public void run(){Bukkit.getPlayer(player).addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 20 * 60, 0));Bukkit.getPlayer(player).addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 20 * 60, 0));}});
			}
		}, 20 * 3L);
		
		
	}

}
