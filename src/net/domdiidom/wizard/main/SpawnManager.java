package net.domdiidom.wizard.main;

import java.util.HashMap;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class SpawnManager {
	
	public static HashMap<String, SpawnOverwatch> hash = new HashMap<String, SpawnOverwatch>();
	static Main main;
	
	public static void onPlayerSpawn(Player player, Main plugin){
		main = plugin;
		
		String spawn = getSpawnPoint();
		
		player.sendMessage("Dein Spawnpunkt ist: " + spawn);
		Location newloc = new Location(player.getWorld(), main.getConfig().getDouble("map."+spawn+".x"), main.getConfig().getDouble("map."+spawn+".y"), main.getConfig().getDouble("map."+spawn+".z"), main.getConfig().getInt("map."+spawn+".yaw"), 0);
		player.teleport(newloc);
	}
	
	@SuppressWarnings("deprecation")
	private static void onSpawnUnavailable(String spawn){
		hash.get(spawn).b = true;
		Bukkit.getScheduler().scheduleAsyncDelayedTask(main, new Runnable(){
			@Override
			public void run(){
				hash.get(spawn).b = false;
			}
		}, 20 * 5);
	}
	
	private static String getSpawnPoint(){
		String spawn = "";
		Random random = new Random();
		int i = random.nextInt(7);
		if(i == 0)
			spawn = "a";
		else if(i == 1)
			spawn = "b";
		else if(i == 2)
			spawn = "c";
		else if(i == 3)
			spawn = "d";
		else if(i == 4)
			spawn = "e";
		else if(i == 5)
			spawn = "f";
		else if(i == 6)
			spawn = "g";
		else if(i == 7)
			spawn = "h";
		
		return spawn;
	}

}
