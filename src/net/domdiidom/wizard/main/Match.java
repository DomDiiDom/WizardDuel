package net.domdiidom.wizard.main;

public class Match {
	String playerA, playerB;
	int arena;
	
	public Match(int arena){
		this.arena = arena;
	}
	
	public void setPlayerA(String name){
		this.playerA = name;
	}
	
	public void setPlayerB(String name){
		this.playerB = name;
	}
	
	public String getPlayerA(){
		return playerA;
	}
	
	public String getPlayerB(){
		return playerB;
	}

}
