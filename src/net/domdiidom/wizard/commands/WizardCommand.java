package net.domdiidom.wizard.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.domdiidom.wizard.main.Main;

public class WizardCommand implements CommandExecutor {
	Main main;
	
	public WizardCommand(Main main){
		this.main = main;
		}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player = (Player) sender;
		if(args[0].equals("spawn")){
			if(args[1].equals("a")){
				main.getConfig().set("map.a.world", player.getLocation().getWorld().getName());
				main.getConfig().set("map.a.x", player.getLocation().getBlockX());
				main.getConfig().set("map.a.y", player.getLocation().getBlockY());
				main.getConfig().set("map.a.z", player.getLocation().getBlockZ());
				main.getConfig().set("map.a.yaw", player.getLocation().getYaw());
				main.saveConfig();
			}
			if(args[1].equals("b")){
				main.getConfig().set("map.b.world", player.getLocation().getWorld().getName());
				main.getConfig().set("map.b.x", player.getLocation().getBlockX());
				main.getConfig().set("map.b.y", player.getLocation().getBlockY());
				main.getConfig().set("map.b.z", player.getLocation().getBlockZ());
				main.getConfig().set("map.b.yaw", player.getLocation().getYaw());
				main.saveConfig();
			}
			if(args[1].equals("c")){
				main.getConfig().set("map.c.world", player.getLocation().getWorld().getName());
				main.getConfig().set("map.c.x", player.getLocation().getBlockX());
				main.getConfig().set("map.c.y", player.getLocation().getBlockY());
				main.getConfig().set("map.c.z", player.getLocation().getBlockZ());
				main.getConfig().set("map.c.yaw", player.getLocation().getYaw());
				main.saveConfig();
			}
			if(args[1].equals("d")){
				main.getConfig().set("map.d.world", player.getLocation().getWorld().getName());
				main.getConfig().set("map.d.x", player.getLocation().getBlockX());
				main.getConfig().set("map.d.y", player.getLocation().getBlockY());
				main.getConfig().set("map.d.z", player.getLocation().getBlockZ());
				main.getConfig().set("map.d.yaw", player.getLocation().getYaw());
				main.saveConfig();
			}
			if(args[1].equals("e")){
				main.getConfig().set("map.e.world", player.getLocation().getWorld().getName());
				main.getConfig().set("map.e.x", player.getLocation().getBlockX());
				main.getConfig().set("map.e.y", player.getLocation().getBlockY());
				main.getConfig().set("map.e.z", player.getLocation().getBlockZ());
				main.getConfig().set("map.e.yaw", player.getLocation().getYaw());
				main.saveConfig();
			}
			if(args[1].equals("f")){
				main.getConfig().set("map.f.world", player.getLocation().getWorld().getName());
				main.getConfig().set("map.f.x", player.getLocation().getBlockX());
				main.getConfig().set("map.f.y", player.getLocation().getBlockY());
				main.getConfig().set("map.f.z", player.getLocation().getBlockZ());
				main.getConfig().set("map.f.yaw", player.getLocation().getYaw());
				main.saveConfig();
			}
			if(args[1].equals("g")){
				main.getConfig().set("map.g.world", player.getLocation().getWorld().getName());
				main.getConfig().set("map.g.x", player.getLocation().getBlockX());
				main.getConfig().set("map.g.y", player.getLocation().getBlockY());
				main.getConfig().set("map.g.z", player.getLocation().getBlockZ());
				main.getConfig().set("map.g.yaw", player.getLocation().getYaw());
				main.saveConfig();
			}
			if(args[1].equals("h")){
				main.getConfig().set("map.h.world", player.getLocation().getWorld().getName());
				main.getConfig().set("map.h.x", player.getLocation().getBlockX());
				main.getConfig().set("map.h.y", player.getLocation().getBlockY());
				main.getConfig().set("map.h.z", player.getLocation().getBlockZ());
				main.getConfig().set("map.h.yaw", player.getLocation().getYaw());
				main.saveConfig();
			}
			if(args[1].equals("s")){
				main.getConfig().set("map.s.world", player.getLocation().getWorld().getName());
				main.getConfig().set("map.s.x", player.getLocation().getBlockX());
				main.getConfig().set("map.s.y", player.getLocation().getBlockY());
				main.getConfig().set("map.s.z", player.getLocation().getBlockZ());
				main.getConfig().set("map.s.yaw", player.getLocation().getYaw());
				main.saveConfig();
			}
			if(args[1].equals("super")){
				main.getConfig().set("map.super.world", player.getLocation().getWorld().getName());
				main.getConfig().set("map.super.x", player.getLocation().getBlockX());
				main.getConfig().set("map.super.y", player.getLocation().getBlockY());
				main.getConfig().set("map.super.z", player.getLocation().getBlockZ());
				main.getConfig().set("map.super.yaw", player.getLocation().getYaw());
				main.saveConfig();
			}
		}
		return true;
	}

}
