package net.domdiidom.wizard.events;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Fireball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import net.domdiidom.wizard.main.Main;
import net.domdiidom.wizard.main.PlayerStats;
import net.domdiidom.wizard.main.Super;

public class EventListener implements Listener{
	
	Main main;
		
	public EventListener(Main main){
		this.main = main;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerRightClick(PlayerInteractEvent event){
		try{
			if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
				try{
					if(event.getItem().getItemMeta().getDisplayName().equals("�3JOIN")){
						main.stats.get(event.getPlayer()).onPlayerJoinMatch();
					}
					if(event.getItem().getItemMeta().getDisplayName().equals("�4LEAVE")){
						event.getPlayer().teleport(event.getPlayer().getWorld().getSpawnLocation());
						Inventory inventory = event.getPlayer().getInventory();
						inventory.clear();
						ItemStack item = new ItemStack(Material.SLIME_BALL);
						ItemMeta meta = item.getItemMeta();
						meta.setDisplayName("�3JOIN");
						item.setItemMeta(meta);
						inventory.setItem(8, item);
					}
				}catch(Exception e){}
				if(event.getItem().getType() == Material.BLAZE_ROD && main.stats.get(event.getPlayer()).isFireballReady && event.getPlayer().getLevel() >= main.getConfig().getInt("spell.fireball.mana")){
					main.stats.get(event.getPlayer()).onUseFireball();
					event.getPlayer().launchProjectile(Fireball.class);
				}else if(event.getItem().getType() == Material.SHIELD && event.getPlayer().getLevel() >= main.getConfig().getInt("spell.shield.mana") && main.stats.get(event.getPlayer()).isShieldReady){
					main.stats.get(event.getPlayer()).onUseShield();
					
					Location loc = event.getPlayer().getLocation();
					String dir = Main.getCardinalDirection(event.getPlayer());
					if(dir.equals("S")){
						main.South(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
					}else if(dir.equals("E")){
						main.East(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
					}else if(dir.equals("W")){
						main.West(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
					}else if(dir.equals("N")){
						main.North(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
					}else if(dir.equals("SE")){
						main.South(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
						main.East(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
					}else if(dir.equals("NE")){
						main.North(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
						main.East(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
					}else if(dir.equals("NW")){
						main.North(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
						main.West(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
					}else if(dir.equals("SW")){
						main.South(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
						main.West(event.getPlayer().getWorld(), loc);
						main.onSpawnShield();
					}
				}else if(event.getItem().getType() == Material.FEATHER && event.getPlayer().getLevel() >= main.getConfig().getInt("spell.teleportation.mana") && main.stats.get(event.getPlayer()).isTeleportationReady){
					main.stats.get(event.getPlayer()).onUseTeleportation();
					int range = main.getConfig().getInt("spell.teleportation.range");
					Location loc = event.getPlayer().getLocation();
					int y = loc.getBlockY();
					Vector dir = loc.getDirection();
					dir.normalize();
					dir.multiply(range);
					loc.add(dir);
					loc.setY(y);
					event.getPlayer().teleport(loc);
				}
			}
		}catch(Exception e){e.printStackTrace();}		
	}
	
	@EventHandler
	public void onBlockPlaced(BlockPlaceEvent event){
		if(!event.getPlayer().hasPermission("wizard.admin"))
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		if(!event.getPlayer().hasPermission("wizard.admin"))
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event){
		if(!event.getPlayer().hasPermission("wizard.admin"))
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onPrjectilHit(ProjectileHitEvent event){
	        BlockIterator bi = new BlockIterator(event.getEntity().getWorld(), event.getEntity().getLocation().toVector(), event.getEntity().getVelocity().normalize(), 0, 4);
	        Block hit = null;

	        while(bi.hasNext())
	        {
	            hit = bi.next();
	            if(hit.getTypeId()!=0)
	            {
	                break;
	            }
	        }
	        if(hit.getType() == Material.DIAMOND_BLOCK){
	        	hit.setType(Material.AIR);
	        	hit.getLocation().getWorld().createExplosion(hit.getLocation(), 0.1F);
	        }
	}
	
	@EventHandler
	public void onEntityExplosion(EntityExplodeEvent event){
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event){
		main.stats.remove(event.getPlayer());
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		if(!event.getPlayer().hasPermission("wizard.admin"))
			event.getPlayer().teleport(new Location(event.getPlayer().getWorld(), main.getConfig().getDouble("map.s.x"), main.getConfig().getDouble("map.s.y"), main.getConfig().getDouble("map.s.z"), main.getConfig().getInt("map.s.Yaw"), 0));
		event.getPlayer().getInventory().clear();
		main.stats.put(event.getPlayer(), new PlayerStats(main, event.getPlayer()));
		Inventory inventory = event.getPlayer().getInventory();
		ItemStack item = new ItemStack(Material.SLIME_BALL);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("�3JOIN");
		item.setItemMeta(meta);
		inventory.setItem(8, item);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event){
		event.getEntity().getInventory().clear();
		
		Inventory inventory = event.getEntity().getInventory();
		inventory.clear();
		ItemStack item = new ItemStack(Material.SLIME_BALL);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("�3JOIN");
		item.setItemMeta(meta);
		inventory.setItem(8, item);
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event){
		int x = event.getPlayer().getLocation().getBlockX();
		int y = event.getPlayer().getLocation().getBlockY();
		int z = event.getPlayer().getLocation().getBlockZ();
		if(main.superX == x && main.superY == y && main.superZ == z){
			if(!Super.isCharging)
				Super.onEnabled(event.getPlayer().getName(), main);
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event){
		event.setCancelled(true);
	}
	

}
